# README #

### Orion Ansible Test ###

* This is an ansible repository with a playbook to build 3 AWS EC2 Instances each one with a Cassandra Docker Container. 
* One of them is configured as a Master and 2 of them are Slaves.

A general Architecture Diagram of this is as follows:

![Cassandra Cluster on EC2 instances](https://www.lucidchart.com/publicSegments/view/77c37bbe-bb62-4c56-8341-60f07183c7f5/image.jpeg)

### Configuration Instructions ###

* Summary of set up
	* This Playbook is using a EC2 Dynamic Inventory, so no need to deal with Hosts Files.
	* You will need to create a User on AWS to get a AWS Access Keys and Secret Key. Once you get them, set them as env vars:
		``` export AWS_ACCESS_KEY_ID="Aadkjshdkjah" ```
		``` export AWS_SECRET_ACCESS_KEY="+aslkdalkjsdalkjlskd" ```
	* On the /group_vars/all.yml file, edit the value of `aws_subnet_id` to your default subnet id.
	* Once you are done, go to the root directory of the project and run the playbook: `ansible-playbook site.yml`.
	
On your play recap, you should now see 3 instances created each of one with its own configuration running Cassandra.

```
PLAY RECAP **************************************************************************************
34.215.49.160              : ok=7    changed=6    unreachable=0    failed=0
34.216.7.53                : ok=7    changed=6    unreachable=0    failed=0
52.25.73.255               : ok=7    changed=6    unreachable=0    failed=0
localhost                  : ok=11   changed=8    unreachable=0    failed=0
```

You can now ssh into one of the EC2 Instances and take a look at Cassandra Docker containers like this:

```
ssh -i ansible_ec2_dockerswarm_cassandra/credentials/orion-key.pem ec2-user@[host_ip]
```

Once you are inside of the EC2 instance:

If you are connecting to the `manager-instance` run this
```
docker exec -it master-cassandra bash
```
or if you are connecting to one of the two `worker-instance` run
```
docker exec -it slave-cassandra bash
```

You are now inside of the Cassandra container and from here you can check the status of the Cluster like this:
```
nodetool status
```

You should be able to see this:
```
Datacenter: datacenter1
=======================
Status=Up/Down
|/ State=Normal/Leaving/Joining/Moving
--  Address       Load       Tokens       Owns (effective)  Host ID                               Rack
UN  172.31.19.75  165.67 KiB  256          100.0%            59bcc88e-3b26-49ed-bf18-e5998a6f2022  rack1
UJ  52.25.73.255  50.51 KiB  256          ?                 9aa50557-7b17-4f39-8812-8eb67e7617a7  rack1
UJ  34.215.49.160  50.51 KiB  256          ?                 3ie34tt2-1n23-561f-2122-a68d8gf423h5 rack1
```

To run `CQSL` you  have to ssh into any of the EC2 instances and run this command:

```
sudo docker run -it --link [master | slave]-cassandra:cassandra --rm cassandra cqlsh cassandra
```
Note: replace [master | slave] with the appropiate value according to what Host you are connected to. If it's a worker instance, then write slave-cassandra otherwise master-cassandra.
